#! /usr/bin/RSCRIPT

library(caTools)
library(compiler)
library(dplyr)
library(getopt)
library(ggplot2)
library(magrittr)
library(readr)
library(tibble)
library(tidyr)

# Parse command-line arguments.

args <- tribble(
~Long, ~Short, ~Argument, ~Type,
'route', 'r', 1, 'character',
'stations', 's', 1, 'character',
'train', 't', 1, 'character',
'max-speed', 'v', 1, 'numeric',
'initial-acceleration', 'a', 1, 'numeric',
'power-to-weight-ratio', 'k', 1, 'numeric',
'track-resistance', 'f', 1, 'character',
'train-length', 'l', 1, 'numeric',
'direction', 'd', 1, 'character',
'calls', 'c', 1, 'character',
'discrete-length', 'x', 1, 'numeric',
'output-disjoint-route-table', 'j', 1, 'character',
'output-timetable', 'b', 1, 'character',
'output-graph', 'g', 1, 'character',
'output-width', 'w', 1, 'numeric',
'output-height', 'h', 1, 'numeric',
'cant-deficiency', NA, 1, 'numeric',
'track-unit', NA, 1, 'character',
'speed-unit', NA, 1, 'character',
'curve-unit', NA, 1, 'character',
'gradient-unit', NA, 1, 'character',
'cant-unit', NA, 1, 'character',
'track-gauge', NA, 1, 'character'
                ) %>%
  as.matrix() %>%
  getopt()

# Check that some form of output has been chosen.

if (all(is.null(c(args$`output-disjoint-route-table`,
                  args$`output-timetable`,
                  args$`output-graph`)))) {
  stop('No form of output chosen.')
}

# Gravitational acceleration, in m/s^2
gravity <- 9.80665

# Set up train.
if (!is.null(args$train)) {
  # Read in train file, if requested.
  source(args$train)
  # Check that train accel and brake functions are defined.
  for (var in c('accel', 'brake')) {
    if (!exists(var)) {
      error(sprintf('Train file does not define "%s".', var))
    }
  }
  # Overwrite trainlength with command-line specification, if any.
  if (!is.null(args$`train-length`)) {
    warning('Command-line train length overrides definition in train file.')
    trainlength <- args$`train_length`
  }
} else {
  # Check acceleration.
  if (is.null(args$`initial-acceleration`)) {
    stop('No initial acceleration given.')
  }
  if (is.null(args$`power-to-weight-ratio`)) {
    stop('No power-to-weight-ratio given.')
  }
  if (is.null(args$`track-resistance`)) {
    stop('No track resistance given.')
  }
  track_res <- as.numeric(strsplit(args$`track-resistance`, ',')[[1]])
  if (any(is.na(track_res)) | length(track_res) != 3) {
    stop('Track resistance improperly specified.')
  }
  init_accel <- args$`initial-acceleration`
  ptw <- args$`power-to-weight-ratio`
  accel <- function(speed, sin_gradient) {
    # Maximum acceleration limited by gradient and dynamic friction
    if (speed == 0) {
      return(init_accel)
    } else {
      dyn_accel <- (ptw/speed) -
        (track_res[1]) - 
        (track_res[2]*speed) - 
        (track_res[3]*speed*speed) -
        gravity*sin_gradient
      return(min(dyn_accel, init_accel))
    }
  }
  brake <- function(speed, sin_gradient) {
    # Maximum braking limited by gradient and dynamic friction
    if (speed == 0) {
      return(init_accel)
    } else {
      dyn_accel <- ptw/speed +
        track_res[1] +
        track_res[2]*speed + 
        track_res[3]*speed*speed +
        gravity*sin_gradient
      return(min(dyn_accel, init_accel))
    }
  }
}

# Check that train length is now defined.
if (!exists('trainlength')) {
  warning('Train length not defined on command line or in train file; defaulting to zero.')
  trainlength <- 0
}

# Read in route spreadsheet.
route <- read_csv(args$route)
route_colnames <- colnames(route)

# Check integrity of required columns.
for (colname in c('Start', 'End')) {
  if (!colname %in% route_colnames) {
    stop(sprintf('Route spreadsheet has no column "%s".', colname))
  }
  if (any(is.na(route[[colname]]))) {
    stop(sprintf('Route spreadsheet has missing entries in "%s".', colname))
  }
}

# Check that starts are before ends, and fix if not.
if (nrow(filter(route, Start > End)) != 0) {
  warning('Route spreadsheet has some reversed segment endpoints.')
  tmp <- route$Start
  route$Start <- pmin(route$Start, route$End)
  route$End <- pmax(route$End, tmp)
}

# Fill in dummies for missing optional columns.
opt_route_cols <- c('Speed', 'Gradient', 'Curve', 'Cant', 'CantDeficiency')
for (colname in setdiff(opt_route_cols, route_colnames)) {
  # Warn about missing speed column
  if (colname == 'Speed') {
    warning(sprintf('Route spreadsheet has no column "%s".', colname))
  }
  route[[colname]] <- NA
}

# List of directionally specific columns
dir_route_cols <- c(paste('Down', opt_route_cols, sep=''),
                    paste('Up', opt_route_cols, sep=''))

# Warn about extra columns.
for (colname in setdiff(route_colnames,
                        c('Start', 'End', opt_route_cols, dir_route_cols))) {
  warning(sprintf('Route spreadsheet has unrecognized column "%s"', colname))
}
  

# Create empty directional speed and gradient columns if needed.
for (colname in dir_route_cols) {
  if (!colname %in% route_colnames) {
    route[[colname]] <- NA
  }
}

# Fill out missing entries in directional columns with nondirectional entries.
for (col in opt_route_cols) {
  for (dircol in paste(c('Up', 'Down'), col, sep='')) {
    route[[dircol]] <-
      ifelse(is.na(route[[dircol]]), route[[col]], route[[dircol]])
  }
}

# Create table of disjoint route segments.
# First, calculate route breakpoints.
route_breakpoints <- sort(unique(c(route$Start, route$End)))
num_segments <- length(route_breakpoints)-1
# Get list of segment starts, *except* zero-length segments.
segment_starts <- route_breakpoints[1:num_segments]
segment_ends <- route_breakpoints[2:(num_segments+1)]
# Add zero-length segments back in
zero_length_segments <- unique(filter(route, Start == End)$Start)
segment_starts <- sort(c(segment_starts, zero_length_segments))
segment_ends <- sort(c(segment_ends, zero_length_segments))
num_segments <- length(segment_starts)

# Set up list of vectors with speed, gradient, etc., per disjoint segment.
disjoint_segment_data <- list()
for (col in dir_route_cols) {
  disjoint_segment_data[[col]] <- as.numeric(num_segments)
}

for (i in (1:num_segments)) {
  # List all segments in original route file that contain shorter segment
  containing_segments <-
    filter(route, Start <= segment_starts[i] & segment_ends[i] <= End)
  # Data for short segments are taken from the last-listed segment in the
  # original route file that contains the segment and has data given.
  for (col in dir_route_cols) {
    last_non_missing <- last(na.omit(containing_segments[[col]])) 
    disjoint_segment_data[[col]][i] <-
      ifelse(is.null(last_non_missing), NA, last_non_missing)
  }
}

# Replace route table with new disjoint route table.
route <- tibble(Start=segment_starts, End=segment_ends)
for (col in dir_route_cols) {
  route[[col]] <- segment_speed_gradient[[col]]
}

# Sort segments in increasing order of milepost.
route <- arrange(route, Start)

# Write out disjoint route table, if requested.
if (!is.null(args$`output-disjoint-route-table`)) {
  write_csv(route, args$`output-disjoint-route-table`, na='')
  # No sense in doing the simulation if the route table is the *only* output.
  if (is.null(args$`output-timetable`) & is.null(args$`output-graph`)) {
    stop('Only output is route table; quitting without running simulation.')
  }
}

# Get train direction from command line.
if (is.null(args$direction)) {
  warning("No direction specified; defaulting to down.")
  down <- T
} else if (args$direction == 'up') {
  down <- F
} else if (args$direction == 'down') {
  down <- T
} else {
  stop(sprintf('Unrecognized direction: "%s".', args$direction))
}

# Give columns for the train's direction generic names (e.g. "DownSpeed" ->
# "Speed" for a down train), and ditch remaining columns.

for (col in opt_route_cols) {
  dir_col <- paste(ifelse(down, 'Down', 'Up'), col, sep='')
  route[[col]] <- route[[dir_col]]
}

route <- select_(route, .dots=c('Start', 'End', opt_route_cols))

# Read units from command line; convert all speeds should be in m/s, gradients
# to sines of slopes, and track distances, curve radii, and cant to meters.

# US statute yard length. For international yard, multiply by 0.999 998.
yard_to_meter <- 3600/3937

# Table of length units
length_units <-
  tribble(
~Name, ~Meters,
'mm', .001,
'millimeter', .001,
'millimeters', .001,
'millimetre', .001,
'millimetres', .001,
'cm', .01,
'centimeter', .01,
'centimeters', .01,
'centimetre', .01,
'centimetres', .01,
'in', yard_to_meter/36,
'inch', yard_to_meter/36,
'inches', yard_to_meter/36,
'ft', yard_to_meter/3,
'foot', yard_to_meter/3,
'feet', yard_to_meter/3,
'yd', yard_to_meter,
'yard', yard_to_meter,
'yards', yard_to_meter,
'm', 1,
'meter', 1,
'meters', 1,
'metre', 1, 
'metres', 1,
'chain', 22*yard_to_meter,
'chains', 22*yard_to_meter,
'km', 1000,
'kilometer', 1000,
'kilometers', 1000,
'kilometre', 1000,
'kilometres', 1000,
'mi', 1760*yard_to_meter,
'mile', 1760*yard_to_meter,
'miles', 1760*yard_to_meter
)
stopifnot(!anyDuplicated(length_units$Name))

# Table of speed units in meters per second
speed_units <-
  tribble(
~Name, ~MPS,
'km/h', 1/3.6,
'kph', 1/3.6,
'kilometers-per-hour', 1/3.6,
'kilometres-per-hour', 1/3.6,
'm/s', 1,
'meters-per-second', 1,
'metres-per-second', 1,
'mph', 1760*yard_to_meter/3600,
'mi/h', 1760*yard_to_meter/3600,
'miles-per-hour', 1760*yard_to_meter/3600,
'fps', yard_to_meter/3,
'ft/s', yard_to_meter/3,
'feet-per-second', yard_to_meter/3
)
stopifnot(!anyDuplicated(speed_units$Name))

# First, track distances. track_factor is number of meters per specified unit.
if (is.null(args$`track-unit`)) {
  # Meters by default
  track_factor <- 1
} else if (!tolower(args$`track-unit`) %in% length_units$Name) {
  stop('Invalid --track-unit.')
} else {
  track_factor <-
    filter(length_units, Name == tolower(args$`track-unit`))$Meters
}
route <- mutate(route, Start=Start*track_factor, End=End*track_factor)

# Next, speeds.
if (is.null(args$`speed-unit`)) {
  speed_factor <- 1/3.6 # km/h by default
} else if (!tolower(args$`speed-unit`) %in% speed_units$Name) {
  stop('Unrecognized argument to --speed-unit.')
} else {
  speed_factor <-
    filter(speed_units, Name == tolower(args$`speed-unit`))$MPS
}
route <- mutate(route, Speed=Speed*speed_factor)

# Gradient is a bit more complicated.
if (is.null(args$`gradient-unit`)) {
  route$Gradient <- sin(pi*route$Gradient/200)
} else {
  grad_unit <- tolower(args$`gradient-unit`)
  if (grad_unit == 'percent') {
    route$Gradient <- sin(pi*route$Gradient/200)
  } else if (grad_unit == 'permille') {
    route$Gradient <- sin(pi*route$Gradient/2000)
  } else if (grad_unit %in% c('degree', 'degrees')) {
    route$Gradient <- sin(pi*route$Gradient/180)
  } else if (grad_unit %in% c('sin', 'sine')) {
    # pass
  } else if (grad_unit %in% c('tan', 'tangent')) {
    route$Gradient <- route$Gradient/sqrt(1+route$Gradient**2)
  } else {
    stop('Unrecognized argument to --gradient-unit.')
  }
}

# Convert curve radius to meters
if (is.null(args$`curve-unit`)) {
  # Pass: default is meters.
} else {
  curve_unit <- tolower(args$`curve-unit`)
  if (curve_unit %in% length_units$Name) {
    curve_factor <- filter(length_units, Name == curve_unit)$Meters
    route$Curve <- route$Curve*curve_factor
  } else if (curve_unit %in% c('degree', 'degrees')) {
    fifty_feet_in_meters <- 50/(3*yard_to_meter)
    route$Curve <- fifty_feet_in_meters/sin(pi*route$Curve/360)
  } else {
    stop('Unrecognized argument to --curve-unit.')
  }
}

# Finally, convert cant to meters.
if (is.null(args$`cant-unit`)) {
  # Millimeters by default
  cant_factor <- 1e-3
} else if (!tolower(args$`cant-unit`) %in% length_units$Name) {
  stop('Invalid --cant-unit.')
} else {
  cant_factor <-
    filter(length_units, Name == tolower(args$`cant-unit`))$Meters
}
route <- mutate(route,
                Cant=Cant*cant_factor,
                CantDeficiency=CantDeficiency*cant_factor)

# Get default max speed from command line and check integrity.
max_speed <- args$`max-speed`
if (!is.null(max_speed)) {
  if (max_speed <= 0) {
    stop('Maximum speed must be positive.')
  }
}
if (any(is.na(route$Speed)) & is.null(max_speed)) {
  warning('Assuming route segments with missing speed limits have infinite maximum speed.')
  max_speed <- Inf
}

# Get default max cant deficiency from command line and check integrity.
max_cant_def <- args$`cant-deficiency`
if (!is.null(max_cant_def) && max_cant_def <= 0) {
  stop('Maximum cant deficiency must be positive.')
}
# Check for any curves with missing max cant deficiency
if (is.null(max_cant_def)) {
  max_cant_def <- Inf 
  if (nrow(filter(route, !is.na(Curve) & is.na(CantDeficiency))) > 0) {
  warning('No cant deficiency limit given on command line; ignoring curves with missing cant deficiency.')
  }
}

# Fill in missing speed, gradient, curve, and cant with defaults.
route <-
  mutate(route,
         Speed=ifelse(is.na(Speed), max_speed*speed_factor, Speed),
         Gradient=ifelse(is.na(Gradient), 0, Gradient),
         Curve=ifelse(is.na(Curve), Inf, Curve),
         Cant=ifelse(is.na(Cant), 0, Cant),
         CantDeficiency=ifelse(is.na(CantDeficiency),
                               max_cant_def*cant_factor,
                               CantDeficiency))

# Get track gauge in meters.
if (is.null(args$`track-gauge`)) {
  gauge <- 1.435
} else {
  gauge <- as.numeric(args$`track-gauge`)
  if (is.na(gauge)) {
    gauge <- tolower(args$`track-gauge`)
    if (gauge %in% c('meter', 'metre')) {
      gauge <- 1
    } else if (gauge %in% c('japanese', 'three-foot-six')) {
      gauge <- 1.067
    } else if (gauge == 'standard') {
      gauge <- 1.435
    } else if (gauge == 'russian') {
      gauge <- 1.520
    } else if (gauge == 'iberian') {
      gauge <- 1.668
    } else if (gauge %in% c('indian', 'bart', 'five-foot-six')) {
      gauge <- 1.676
    } else {
      stop('Unrecognized --track-gauge.')
    }
  }
}

# Compute speed limits from curves. Cant is exactly balanced when
# arctan(v^2/curve radius*gravity) = arcsin (superelevation/track gauge)
route <-
  mutate(route,
         CurveSpeed=
           ifelse(Cant+CantDeficiency >= gauge, # never happens realistically
                  Inf,
                  sqrt((Cant+CantDeficiency)*Curve*gravity/
                       sqrt(gauge**2 - (Cant+CantDeficiency)**2))))

# Read in stations file.
stations <- read_csv(args$stations)
# Check required columns
stations_colnames <- colnames(stations)
for (col in c('Location', 'Name')) {
  if (!col %in% stations_colnames) {
    stop(sprintf('Station spreadsheet has no column "%s".', col))
  }
}
# Check spurious columns
for (col in stations_colnames) {
  if (!col %in% c('Name', 'Location', 'Dwell', 'UpDwell', 'DownDwell', 'UpAlignment', 'DownAlignment', 'UpOffset', 'DownOffset')) {
    warning(sprintf('Station spreadsheet has unrecognized column "%s"', col))
  }
}

# Check integrity of name and location.
if (any(is.na(stations$Name))) {
  stop('Stations with missing names.')
}
if (any(is.na(stations$Location))) {
  stop('Stations with missing locations.')
}

# Trim whitespace from station names.
stations <- mutate(stations,Name=trimws(Name))

# Check uniqueness of names.
if (anyDuplicated(stations$Name)) {
  stop(sprintf('Duplicated station names: %s.',
               paste(unique(stations$Name(duplicated(stations$Name))),
                     collapse=', ')))
}
# Check if names have no commas.
stations_with_commas <- stations$Name[grepl(',', stations$Name)]
if (length(stations_with_commas) > 0) {
  stop(sprintf('Station names with commas: %s.',
               paste(stations_with_commas, collapse='; ')))
}

# Add blank optional columns missing in the input.
for (col in setdiff(c('Dwell', 'UpDwell', 'DownDwell', 'DownOffset', 'DownAlignment', 'UpOffset', 'UpAlignment'),
                    stations_colnames))
{
  stations[[col]] <- NA
}

# Fill in missing entries in directional columns.
stations <-
  mutate(stations,
         UpDwell=ifelse(is.na(UpDwell), Dwell, UpDwell),
         DownDwell=ifelse(is.na(DownDwell), Dwell, DownDwell),
         UpOffset=ifelse(is.na(UpOffset), 0, UpOffset),
         DownOffset=ifelse(is.na(DownOffset), 0, DownOffset),
         UpAlignment=ifelse(is.na(UpAlignment), 'middle', UpAlignment),
         DownAlignment=ifelse(is.na(DownAlignment), 'middle', DownAlignment))

# Check integrity of directional alignment.
stations <- mutate(stations,
                   UpAlignment=tolower(trimws(UpAlignment)),
                   DownAlignment=tolower(trimws(DownAlignment)))
bad_alignments <-
  setdiff(c(stations$UpAlignment, stations$DownAlignment),
          c('head', 'middle', 'tail'))
if (length(bad_alignments) != 0) {
  stop(sprintf('Unrecognized station alignments: %s.',
               paste(unique(bad_alignments),
                     collapse=',')))
}

# Compute stopping points for train midpoint.
stations <-
  mutate(stations,
         DownLocation=(Location+DownOffset)*track_factor+
           ifelse(DownAlignment=='head',
                  -1,
                  ifelse(DownAlignment=='tail', 1, 0))*trainlength/2,
         UpLocation=(Location+UpOffset)*track_factor+
           ifelse(UpAlignment=='head',
                  1,
                  ifelse(UpAlignment=='tail', -1, 0))*trainlength/2)

# Choose directionally specific columns
if (down) {
  stations <- select(stations, Name, Location=DownLocation, Dwell=DownDwell)
} else {
  stations <- select(stations, Name, Location=UpLocation, Dwell=UpDwell)
}

# Parse list of calls
if (is.null(args$calls)) {
  warning('No station calls listed; assuming train is all-stops.')
  calls <- stations$Name
} else {
  calls <- strsplit(args$calls, ',')[[1]]
}

# Check that all calls are in the list of stations
for (call in calls) {
  if (!call %in% stations$Name) {
    stop(sprintf('Unrecognized station "%s".', call))
  }
}

# Mark stations table with a list of halts.
stations <- mutate(stations, Calling=(Name %in% calls))

# Now we're finally ready for the simulation, done by numerical integration.
# Set a length of track (in meters) to use as a discrete unit.
# Length quantities measured in terms of this unit will have a "d"
# prefix (e.g. dStart, dEnd)
dx <- ifelse(is.null(args$`discrete-length`), 1, args$`discrete-length`)

# Fill in missing station dwells.
stations <- mutate(stations, Dwell=ifelse(is.na(Dwell), 0, Dwell))

# Generate a new route table with different length units.  Train starts at
# track-location 1 (not zero because R vectors are 1-indexed), track locations
# increase by 1 per dx (including in up direction).  We'll subtract the extra 1
# at the end for the outputs.

# Train's ending location and length in new units.
trainstart <- filter(stations, Calling == T)$Location %>% min()
trainend <- filter(stations, Calling == T)$Location %>% max()
if (!down) {
  tmp <- trainstart
  trainstart <- trainend
  trainend <- tmp
}

# Conversion functions from meter posts on line to discrete length units.
meters_to_discrete <- function(x) {
  as.integer(1+ifelse(down, 1, -1) * (x - trainstart)/dx)
}
discrete_to_meters <- function(x) {
  (x - 1)*dx*ifelse(down, 1, -1) +trainstart
}

d_trainend <- meters_to_discrete(trainend)

# New route table in terms of discrete units.
d_route <-
  mutate(route,
         d_Start=meters_to_discrete(Start),
         d_End=meters_to_discrete(End),
         # Reverse gradient if train is up.
         Gradient=Gradient*ifelse(down, 1, -1))

# Reverse start and end if train is traveling up.
if (!down) {
  d_route <- rename(d_route, d_Start=d_End, d_End=d_Start)
}

d_route <- d_route %>%
  # Drop segments outside train path and truncate others
  filter(!(d_End <= 1 | d_Start >= d_trainend)) %>%
  mutate(d_Start=pmax(d_Start, 1),
         d_End=pmin(d_End, d_trainend))

d_stations <-
  mutate(stations,
         d_Location=meters_to_discrete(Location)) %>%
  arrange(d_Location)

# Number of the train's final discrete length.
d_trainend <- meters_to_discrete(trainend)


# Extract table of midpoints of all speed-limiting curves for future plotting.
speed_limiting_curves <-
  filter(route, CurveSpeed < Speed) %>%
  mutate(d_Midpoint=meters_to_discrete((Start+End)/2)) %>%
  select(d_Midpoint, CurveSpeed)

# Now we'll make a vector whose i-th entry has the train's speed at discrete
# location i. Several steps are required.

# First, make a vector of allowable speeds, in m/s, at each discrete location *for a zero-length train*. We're separating allowable speeds due to speed limits and allowable speeds due to curves.
posted_speeds_by_dx <- numeric(d_trainend)
curve_speeds_by_dx <- numeric(d_trainend)
gradients_by_dx <- numeric(d_trainend)

# Iterate through d_route, getting speed limits and gradients for each range of discrete locations.

for (i in (1:num_segments)) {
  # "Posted" speed limit
  posted_speed <- d_route$Speed[i]
  # Speed limits forced by curves
  curve_speed <- d_route$CurveSpeed[i]
  gradient <- d_route$Gradient[i]
  start <- d_route$d_Start[i]
  # Give each segment except the last an "open" right endpoint so as not to overwrite zero-length segments.
  end <- d_route$d_End[i] - ifelse(i == num_segments, 0, 1)
  if (!is.na(posted_speed)) {
    posted_speeds_by_dx[start:end] <- posted_speed
  }
  if (!is.na(curve_speed)) {
    curve_speeds_by_dx[start:end] <- curve_speed
  }
  if (!is.na(gradient)) {
    gradients_by_dx[start:end] <- gradient
  }
}

# Get all train maximum speeds.
speeds_by_dx <- pmin(posted_speeds_by_dx, curve_speeds_by_dx)

# Adjust speeds_by_dx to account for train length near speed restrictions.
speeds_by_dx <- runmin(speeds_by_dx, trainlength/dx, align='center')

# Set speeds at each station stopped at to zero.
for (i in filter(d_stations, Calling == T)$d_Location) {
  speeds_by_dx[i] <- 0
}

# Adjust to account for acceleration out of slow zones and up gradients.
# Recall: uniform acceleration a over length dx =>
# vfinal = sqrt(vinit^2 + 2*a*dx) approx= vinit + a*dx/vinit when x is small

# First, compile acceleration function.
accel_comp <- cmpfun(accel)

# Adjust speeds to account for maximum acceleration.
# If trains lose momentum going up gradients, that will show up here, too.
for (i in 1:(d_trainend-1)) {
  accel_inst <- accel_comp(speeds_by_dx[i], gradients_by_dx[i])
  speeds_by_dx[i+1] <- min(speeds_by_dx[i+1], 
                           sqrt(speeds_by_dx[i]**2 + 2*accel_inst*dx))
}

# Same thing for braking in advance of speed restrictions.
brake_comp <- cmpfun(brake)
for (i in d_trainend:2) {
  brake_inst <- brake_comp(speeds_by_dx[i], gradients_by_dx[i])
  # Counterintuitively, trains may need to slow down in advance of downhills
  # if their braking is poor enough that they'd be pulled faster than the
  # speed limit.
  speeds_by_dx[i-1] <- min(speeds_by_dx[i-1],
                           sqrt(speeds_by_dx[i]**2 + 2*brake_inst*dx))
}

# Now compute travel times (in seconds) per segment.
# dtimes[i] == travel time from location i-1 to location i
times_by_dx <- as.numeric(d_trainend-1)
times_by_dx[1] <- 0
for (i in 2:d_trainend) {
  times_by_dx[i] = 2*dx/(speeds_by_dx[i] + speeds_by_dx[i-1])
}

# Add dwell times for intermediate stations
d_stations_calling <- filter(d_stations, Calling == T)
if (nrow(d_stations_calling) >= 3) {
  for (i in 2:(nrow(d_stations_calling)-1)) {
    times_by_dx[d_stations_calling$d_Location[i]+1] <-
      times_by_dx[d_stations_calling$d_Location[i]+1] +
      d_stations_calling$Dwell[i]
  }
}

# Create final data table for plotting, converting speeds back to km/h.
simulation <-
  tibble(d_Location=(1:d_trainend),
         SegmentTime=times_by_dx,
         Speed=speeds_by_dx*3.6) %>%
  mutate(ElapsedTime=cumsum(SegmentTime),
         Meter=discrete_to_meters(d_Location))

# Exract arrival times at each station
station_times <-
  left_join(d_stations, simulation, by='d_Location') %>%
  select(Name, d_Location, ElapsedTime, Location, Dwell, Calling, Meter) %>%
  mutate(Timestamp=sprintf("%d:%02d",
                           as.integer(ElapsedTime/60),
                           as.integer(ElapsedTime %% 60))) %>%
  mutate(Label=sprintf(ifelse(Calling, "%s\n(%.1f km)\n%s", "[%s]\n(%.1f km)\n%s"), Name, Meter/1000, Timestamp),
         # Zero-index location
         d_Location=d_Location-1)

# Write out table with simulation, if requested.

if (!is.null(args$`output-timetable`)) {
  stations_to_write <-
    select(d_stations, d_Location, Station=Name, Dwell, Calling) %>%
    mutate(Dwell=ifelse(Calling, Dwell, NA)) %>%
    select(-Calling)
  # Remove dwell times from first and last stations
  stations_to_write$Dwell <- 
    c('Terminus',
      as.character(stations_to_write$Dwell[2:(nrow(stations_to_write)-1)]),
      'Terminus'
      )
  simulation_to_write <-
    left_join(simulation, stations_to_write, by='d_Location') %>%
    select(-SegmentTime, -d_Location)
  write_csv(simulation_to_write, args$`output-timetable`, na='')
}

# Make plot.

if (!is.null(args$`output-graph`)) {
  # Compute breaks for elapsed-time axis
  top_speed <- max(simulation$Speed)
  elapsed_minutes <- max(simulation$ElapsedTime)/60
  time_breaks <- pretty(c(0, elapsed_minutes))
  time_labels <- sprintf("%d:%02d",
                         as.integer(time_breaks),
                         as.integer((time_breaks*60) %% 60))

  simulation_to_plot <-
    select(simulation, d_Location, ElapsedTime, Speed) %>%
    mutate(SpeedLimit=posted_speeds_by_dx*3.6,
           CurveLimit=curve_speeds_by_dx*3.6,
           ScaledElapsedTime=ElapsedTime*top_speed/(60*elapsed_minutes)) %>%
    select(d_Location, Speed, SpeedLimit, ScaledElapsedTime) %>%
    gather(series, value, -d_Location) %>%
    mutate(series=factor(series,
                         levels=c('Speed', 'SpeedLimit', 'ScaledElapsedTime')),
           d_Location=d_Location-1) # Zero-index locations

  # Color for elapsed time line and axis
  time_color <- 'red'

  gg <- ggplot() +
    geom_line(data=simulation_to_plot,
              aes(x=d_Location, y=value, group=series, color=series, linetype=series)) +
    theme_bw() +
    theme(legend.position='none',
          axis.text=element_text(color='black'),
          panel.grid.major.x=element_line(color='gray80'),
          panel.grid.major.y=element_line(color='gray80'),
          panel.grid.minor=element_blank(),
          panel.border=element_blank(),
          axis.ticks=element_blank(),
          axis.text.y.right=element_text(color=time_color),
          axis.ticks.y.right=element_line(color=time_color),
          axis.line.y.right=element_line(color=time_color),
          plot.margin=margin(t=0, b=0, r=1, l=0, unit='cm')) +
    scale_x_continuous(breaks=station_times$d_Location,
                       labels=station_times$Label,
                       expand=c(0,0)) +
    scale_y_continuous(expand=c(0,1),
                       sec.axis = sec_axis(~. * elapsed_minutes / top_speed,
                                           name='Elapsed time',
                                           breaks=time_breaks,
                                           labels=time_labels)) +
    labs(x=NULL, y="Kilometers per hour") +
    scale_linetype_manual(values=c('solid', 12, 'solid')) +
    scale_color_manual(values=c('black', 'black', time_color)) +
    geom_hline(yintercept=0, color='black') +
    geom_point(data=speed_limiting_curves,
               aes(x=d_Midpoint, y=CurveSpeed*3.6),
               color='blue', size=4)

  height <- ifelse(!is.null(args$`output-height`),
                   args$`output-height`,
                   15)
  width <- ifelse(!is.null(args$`output-width`),
                  args$`output-width`,
                  25)
  png(args$`output-graph`, width=width, height=height, res=150, units='cm')
  print(gg)
  dev.off()
}
