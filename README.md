# Train performance simulator

A simulator of train performance along routes with varying gradients and speed
restrictions. It is meant to require minimal programming knowledge.

## How to run

### Requirements

A recent R version with the packages `caTools`\*, `compiler`\*,
`dplyr`, `getopt`\*, `ggplot2`, `magrittr`, `readr`, `tibble`, and `tidyr`. All
packages except the starred ones are in the
[Tidyverse](https://www.tidyverse.org).

### Invocation

Invocation follows standard Unix command-line syntax, e.g.: 
```
./trainsim.R --route=route.csv --stations=stations.csv --train=train.R
[--direction=down] [--calls="stop1,stop2,stop3"] [--output-graph=train.png]
```
(possibly with spaces instead of equals signs); or, using short options:
```
./trainsim.R -r route.csv -s stations.csv -t train.R [-d down] [-c
stop1,stop2,stop3] [-g train.png]
```

The command-line options fall into five categories:

1) Route specifications,
2) Train specifications,
3) Stopping patterns,
4) Control over the simulation itself, and
5) Output control.

Each of these will be dealt with in turn.

#### Side note: direction conventions

This document uses the following terms for rail directions:
- **Down**: towards higher mileposts.
- **Up**: towards lower mileposts.

This perhaps counterintuitive terminology is adapted from UK conventions.

### Route specifications

#### --route|-r

A path to a CSV route description. Each row of the CSV represents a segment of
the route and must specify the following columns:

- `Start`: location of start of segment, in meters unless otherwise specified
  with `--track-unit`.
- `End`: location of end of segment, ditto.

There is no way to specify "mileage breaks" (e.g. multiple zero mileposts, or
an inconsistency resulting from the shortening of one route segment without
correcting the subsequent mileposts). Users must ensure that mileposts
locations are consistent. Mileposts may be negative.

The following columns may also be specified:
- `Speed`: maximum speed, default infinity, in km/h unless otherwise specified
  with the `--speed-unit` option.
- `Gradient`: slope in the down direction (and, by default, the negative of the
  slope in the down direction), in percent unless otherwise specified with the
`--gradient-unit` option. Positive values mean that down trains encounter
uphills and up trains encounter downhills; negative values mean the reverse.
- `Curve`: Curve radius, in meters unless otherwise specified with
  `--curve-unit`, default infinity (i.e., straight track).
- `Cant`: Track cant, in millimeters unless otherwise specified with
  `--cant-unit`, default zero.
- `CantDeficiency`: maximum allowable cant deficiency *as experienced by the
  train* (not, in the case of tilting trains, by the passengers). Units are the
same as in `Cant`. Values specified here override any values specified with the
`--cant-deficiency` command-line option. The ability to specify cant deficiency
in the route file is intended to facilitate simulations which tilting is
disabled for portions of a route, for international trains that cross between
countries with different standards, and similar.

A missing `Speed` column will be warned about but will not cause an error.
Missing `Gradient`, `Curve`, or `Cant` will be filled in with default values
silently.

Optionally, different speeds, gradients, and curve properties may be given for
down and up trains, in optional columns `DownSpeed` and `UpSpeed` for speed
limits, `DownGradient` or `UpGradient` for gradients, and likewise for `Curve`,
`Cant`, and `CantDeficiency`. If specified, entries in these columns overwrite
(not modify) `Speed` and `Gradient`. `UpGradient` has the same sign conventions
as `Gradient`: positive values `UpGradient` mean that up trains encounter
downhills, and positive values of `UpCurve` still mean that up trains turn
clockwise.

If *any* part of a train is in a speed restriction, then the train travels at
the restricted speed. For example, a 200-meter-long train traveling through a 1 km
speed restriction needs to slow down for 1.2 km total.

Route segments need not be specified in order and may overlap. If segments
overlap, then information provided for segments lower down in the spreadsheet
overwrites information for overlapping portions of segments listed higher up.
Allowing overlapping segments simplifies specifying routes with a uniform speed
limit except for a few restrictions: all the "normal" segments of the route can
be specified at once rather than being broken into non-overlapping segments.
Route segments may also have zero length (for example, to specify the
controlling point of a curve).

#### --stations|-s

A path to a complete CSV list of stations on the line. Required columns:
- `Name`: name of the station; will be printed on output graphs.
- `Location`: location of the station.  By default, this is where the
  *midpoint* of the train stops. Units are the same as in `Start` and `End` of
the route spreadsheet.

Station names must be unique (and not distinguished merely by leading and
trailing whitespace) and must not contain commas.

Optional columns:
- `DownOffset` and `UpOffset`: for use if down and up platforms are separated.
  The offsets are added to `Location` to compute true platform locations.
Positive `DownOffset` and `UpOffset` *both* locate platforms further to the
"down" end of the line). Defaults to zero.
- `Dwell`: dwell time in seconds; defaults to zero.
- `DownDwell`/`UpDwell`: direction-specific dwell time; if specified,
  overwrites `Dwell`.
- `DownAlignment`/`UpAlignment`: useful for simulations that account for
  varying train lengths. (For example, at Grand Central, trains always stop
against the terminal track buffers whatever their length, so the location of
the train midpoint varies). Possible values:
  - `head`: head of train stops at `Location` plus relevant `Offset`
  - `middle` (default): midpoint of train stops at `Location`, etc.
  - `tail`: tail of train, etc.

So, for example, Grand Central would have `tail` for `DownAlignment` (i.e.,
departing trains) and `head` for `UpAlignment` (i.e., arriving trains). Other
values of `DownAlignment` and `UpAlignment` throw errors; blank entries or
missing columns default to `middle`.

#### --max-speed|-v

Default speed limit for route segments without specified speed limits, in
kilometers per hour unless otherwise specified with `--speed-unit`. Default
infinity.

#### --cant-deficiency

Default maximum cant deficiency for route segments with no specified maximum,
in millimeters unless otherwise specified with `--cant-unit`. If this is not
specified, then curves will be ignored except for segments in the route file
with cant deficiency specified.

#### --track-unit

Units for the `Start` and `End` of track segment locations, as well as the
`Location` and `Offset` of stations. Possible values, in increasing size:
- `mm`/`millimeter(s)`/`millimetre(s)`
- `cm`/`centimeter(s)`/`centimetre(s)`
- `in`/`inch(es)`
- `ft`/`feet`/`foot`
- `yd`/`yard(s)`
- `m`/`meter(s)`/`metre(s)`: default
- `chain(s)` (66 feet = 1 chain = 1/80 mile)
- `km`/`kilometer(s)`/`kilometre(s)`
- `mi`/`mile(s)`

Imperial measurements use the US survey system (based on the conversion 1 meter
= 39.37 inches), not the international system, in which all units are exactly
one part in 500,000 shorter.

#### --speed-unit

Units for speed limits given in the route file, as well as the `--max-speed`
option. Possible values:
- `km/h`/`kph`/`kilomet(er|re)s-per-hour`: default
- `m/s`/`met(er|re)s-per-second`
- `mph`/`mi/h`/`miles-per-hour`
- `fps`/`ft/s`/`feet-per-second`

#### --curve-unit

Units for curve radii. Possible values include any valid value of
`--track-unit`, as well as:
- `degree(s)`: the central angle, in degrees, subtended by a 100-foot chord;
  i.e., 360 arcsin(50 feet/curve radius)/&pi;. This is an old-fashioned American
measurement, roughly proportional to the *inverse* of the radius.

#### --gradient-unit

Units for gradients. Possible values include:
- `percent`: default
- `permille`
- `degree(s)`
- `sin(e)`/`tan(gent)`: gradient entry in route file is interpreted as the sine
  or tangent of the slope.

#### --cant-unit

Unit for measuring track cant. Possible values are the same as `--track-unit`.

#### --track-gauge

Used  with `--cant-unit` to convert cants into angles. Possible values: any number (in millimeters), or:
- `meter`/`metre`: 1000 mm
- `japanese`/`three-foot-six`: 1067 mm
- `standard`: 1435 mm (default)
- `russian`: 1520 mm
- `iberian`: 1668 mm
- `indian`/`bart`/`five-foot-six`: 1676 mm

### Train specifications

Train performance may be specified either in an R code file or in command-line
arguments that give parameters for a standard performance function.  Arguments
must be in the specified units.

#### --train|-t
An R file that defines the following three variables:
 - `accel`: a function that takes train speed (in **m/s**, not km/h) and the
   **sine** of the gradient and returns the train's full-throttle acceleration
(in m/s&sup2;).
 - `brake`: same as `accel`, but for braking. `brake` should return *positive*
   values.
 - `trainlength` [optional]: train length in meters, defaults to zero.

The simulator does not account for limits on acceleration for passenger
comfort, energy conservation, or the like . Likewise, the simulator does not try
to calculate the effect on acceleration of having different parts of the train
on different gradients.

If `--train` is specified, then all other command-line arguments in this
section **except** `--train-length` are ignored. (The exception aims to
facilitate experiments that vary the number of cars of identical type in a
train.)

#### --initial-acceleration|-a

Initial acceleration in m/s&sup2;. This value is dictated by the static
friction between train wheels and rails and holds for both acceleration and
braking unless dynamic resistance (given by `--power-to-weight-ratio` and
`--track-resistance`) specifies a lower limit. Changes in initial acceleration
on gradients, resulting from changes in the normal force between train and
track, are assumed negligible.

#### --power-to-weight-ratio|-k

Power-to-weight ratio in kW/t (equivalently W/kg, or m&sup2;/s&sup3;). The
short argument was chosen for consistency with Alon Levy's own calculator, but
you can think of it as standing for "kilowatts" (or German "Kraft") if you feel
like it.

#### --track-resistance|-f

Three numbers a, b, c, separated by commas, e.g. `".0059,.000118,.000022"`.
Scientific notation with a letter E (upper or lowercase) to denote powers of 10
(e.g., `5.9e-3`) is also possible. These denote, respectively, the constant,
linear, and quadratic terms of dynamic resistance (in units m/s&sup2;,
s<sup>-1</sup>, and m<sup>-1</sup>, respectively). The train's instantaneous
acceleration (resp. deceleration) at velocity v and zero gradient is
kv<sup>-1</sup> - a - bv - cv&sup2; (resp. kv<sup>-1</sup> + a + bv +
cv&sup2;).

An uphill (resp. downhill) gradient reduces (resp. increases) dynamically
limited acceleration, and increases (resp. reduces) dynamically limited
braking, by g sin(&theta;), where g = 9.80665 m/s&sup2; is gravitational
acceleration and &theta; = arctan(gradient/100) is the slope. Downhill
gradients act symmetrically. You may encounter the counterintuitive result of a
train slowing down in advance of a steep decline so that it does not run over
the speed limit as it gets pulled downhill.

The short option `-f` is meant to recall "friction."

#### --train-length|-l

Train length, in meters. Default zero. If specified along with `--train`, this
setting overrides the train file's `trainlength` variable.

### Stopping patterns

#### --direction|-d

Either `down` or `up`. Default `down`.

#### --calls|-c

A comma-separated list of stations at which the train stops (e.g.
"GCT,STM,NHV"), with names corresponding to entries in the file specified by
`--stations`. If stations are named out of order, the order and will be
corrected silently.  The default is every station in the file specified by
`--stations`. The list should be placed in quotation marks if any of the
specified stations contains whitespace or other non-alphanumeric characters.

The word "calls" is a Britishism, used because "stops" and "stations" have the
same first letter.

### Simulation control

Currently, there is only one control of the simulation mechanism itself:

#### --discrete-length|-x

The simulation computes travel times through numeric differential-equation
solving, assuming constant acceleration over small distances.
`--discrete-length` optionally specifies this distance, in meters.  The default
is 1. Internally, station locations and route endpoints are rounded to
multiples of this length.

### Output control

#### --output-disjoint-route-table|-j

Internally, the simulator creates a version of the table specified by `--route`
in which all route segments are disjoint. If this option is invoked, this table
will be written to a CSV file with path given by the option's argument.
Directional speeds and gradients are inferred from the general `Speed` and
`Gradient` if necessary, and any missing speeds and gradients are left as
blank, not filled in with the defaults. This spreadsheet may be useful for
further analysis or 

#### --output-timetable|-b

If specified, a complete record of the train simulation, in the form of a CSV
spreadsheet with the following columns, will be written to the path specified:
- `Meter`: Track location in meters.
- `Speed`: Train speed in km/h.
- `ElapsedTime`: Elapsed time in seconds.
- `Station`: The location of station platforms for the specified direction,
  defined as the stopping location of the train's midpoint, if the specified
discrete location has a station (whether or not the train stops).
- `Dwell`: Dwell time in seconds for intermediate stations at which the train
  halts.

This spreadsheet may be useful for separate analysis or graphing.

#### --output-graph|-g

This will write to the specified filename a PNG file depicting the simulation.
The output graph gives the following:
- Train speeds and speed limits as a function of distance traveled, in black.
- Elapsed time as a function of distance traveled, in red.
- Locations of all stations along the train's route, whether stopped at or
  passed at speed, along with times of arrival or passing.
- Midpoints of all curves that force top speeds lower than the specified speed
  limits, in blue.

#### --output-width|-w

Width of output image, in cm. Default 25.

#### --output-height|-h

Height of output image, in cm. Default 15.

## Current simulator limitations

- The simulator assumes that all speed limits due to curves are caused by the
  curve's tightest point and makes no attempt to control for limits on jerk,
which may limit train speeds further due to tight S-curves or suboptimal superelevation ramps and curve spirals.
- The simulator does not account for limits on cant excess.
- Output customization is extremely limited; in particular, it is
  impossible to customize output (rather than input) units.
